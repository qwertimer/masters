#!/bin/bash

#training Simple CNN 
echo 'training simple CNN'
python __main__.py --config_file=simple.json --experiment_name=simple

echo 'training Residual model'
python __main__.py --config_file=residual.json --experiment_name=residual

echo 'training SE model'
python __main__.py --config_file=SE.json --experiment_name=se_model
###
echo 'training full model'
python __main__.py --config_file=full.json --experiment_name=full
#
echo 'training lstm model'
python __main__.py --config_file=lstm.json --experiment_name=lstm

echo 'training gru model'
python __main__.py --config_file=gru.json --experiment_name=gru

