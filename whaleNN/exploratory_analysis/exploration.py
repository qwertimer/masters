from pathlib import Path
import sys
import os
from easydict import EasyDict as edict
import json
from PIL import Image

from whaleNN.training.lightning_build import whale_classifier as wc
from whaleNN.training.lightning_build import whaleDataModule as whale_dm

import torchaudio

class exploration():

    def __init__(self):
        pass

    def get_dm(self, parameters, hyperparameters):
        dm = whale_dm(self, parameters, hyperparameters)
        return (dm)
    def dataset_size(self, dm):
        y_sum = []
        dm.setup('fit')
        for batch in dm.train_dataloader():
            for x, y in batch:
                y_sum.append(y)
        for batch in dm.val_dataloader():
            for x, y in batch:
                y_sum.append(y)
        dm.setup('test')
        for batch in dm.test_dataloader():
            for x, y in batch:
                y_sum.append(y)
        print(f"y_sum :{y_sum}")
        return (y_sum)

    def get_item():
        pass
    def plot_waveplot(sound):

        #Get single item from each species
        #Convert to Image - PIL.image.show
        return (Image.fromarray(sound))
    def plot_spectrogram(sound):
    #Pass in a sound file to Spectrogram() function
        specgram = torchaudio.transforms.Spectrogram()(sound)
        return (specgram)


def main():

    #Display dataset samples
    params, hparams = get_json()
    print(f" Params : {params} \n HyperParams; {hparams}")
    expl_analysis = exploration()
    dm = expl_analysis.get_dm(params, hparams)
    print(dm)
    y_dataset = dataset_size(dm)
    #Get dataset plot
    #expl_analysis.dataset_size(dataset)
    #Plot a sample waveform for each species
    #plot_waveform(dataset)
    #plot spectrogram of the species
    #plot_spectrogram(dataset)

    #Run traditional analysis
    #k_means_clustering

def get_json():
    exp_path = Path.cwd()
    print(exp_path)
    with open(str(exp_path) + "/config.json",  "r") as f:
        x = json.load(f)
    params = edict(x)
    parameters = params.PARAMETERS
    hyperparameters = params.HYPERPARAMETERS
    return(parameters, hyperparameters)




def get_data():
    data_dir = "/home/tim/SSD/Masters/Datasets/Master Whale Sounds/Master Whale Sounds/kFold/"

    glob.glob(data_dir, recursive = True)



if __name__ == "__main__":
    main()
