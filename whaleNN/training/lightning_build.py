import os
import random
import matplotlib.pyplot as plt
import numpy as np
#Pytorch Basics
import torch
from torch.nn import functional as F
from torch import nn
from torch.utils.data import DataLoader
#from torchvision import transforms
#Audio
import librosa
#import librosa.display
import torchaudio
from scipy import signal

import pytorch_lightning as pl
from pytorch_lightning import metrics as pl_metrics
from pytorch_lightning.metrics.functional import accuracy
from pytorch_lightning.metrics import Precision, Recall, ConfusionMatrix
import pytorch_lightning.metrics.functional.confusion_matrix as conf_matrix
from pytorch_lightning.metrics import F1
#from pytorch_lightning.metrics import Accuracy
#from pytorch_lightning.core.lightning import LightningModule
import torchlayers

from scikitplot.metrics import plot_confusion_matrix

#personal
from whaleNN.models.models import CNN_RNN_model, RNN_model, AutoEncoder, pretrained
from whaleNN.dataset.dataset import whale_dataset
from whaleNN.testing.metrics import metrics_logging

class whale_model_selector():
    def __init__(self):
        print("Running Model Selector")

    def model_call(self, params, hyperparams):
        self.params = params
        self.hparams = hyperparams
        self.model_type = self.hparams["model_type"]     #Type of training simple, SE, RES etc.
        self.classes = self.hparams["classes"]
        print(f"classes :{self.classes}")
        self.input_dim = self.params["input_dim"]
        self.hidden_dim = self.params["hidden_dim"]
        self.model_name = self.params["model_name"]      #For pretrained model selection
        self.size = self.params["size"]

        if self.model_type == 'SIMPLE':

            self.model = torch.nn.Sequential(
                            torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Conv(128, groups = 16),
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Conv(128, groups = 16),
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Conv(128, groups = 16),
                            torchlayers.BatchNorm(),
                            torchlayers.ReLU(),
                            torchlayers.Conv(128, groups = 16),
                            torchlayers.BatchNorm(),

                          torchlayers.GlobalMaxPool(),
                          torchlayers.Linear(self.classes)  # Output for 7 classes
                            )
              #Model with Residual Blocks
        elif self.model_type == 'RES':

            self.model = torch.nn.Sequential(
                            torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Residual(
                                torchlayers.Sequential(
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ReLU(),
                                    torchlayers.GroupNorm(num_groups=4),
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ChannelShuffle(groups=16),
                                    torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.5), #50 % probability
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.5), #50 % probability
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.5), #50 % probability
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2), #50 % probability
                          torchlayers.GlobalMaxPool(),
                          torchlayers.Linear(self.classes)  # Output for 7 classes
                            )

                  #Model with Squeeze and Excitation
        elif self.model_type == 'SE':
            self.model = torch.nn.Sequential(
                            torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                                torchlayers.BatchNorm(),
                                torch.nn.ReLU(),  # use torch.nn wherever you wish
                                torch.nn.Dropout(0.5),
                                torchlayers.SqueezeExcitation(hidden=128),
                                torchlayers.BatchNorm(),
                                torch.nn.ReLU(),  # use torch.nn wherever you wish
                                torch.nn.Dropout(0.5),
                                torchlayers.Conv(128, groups = 16),
                                torchlayers.BatchNorm(),
                                torch.nn.ReLU(),  # use torch.nn wherever you wish
                                torch.nn.Dropout(0.5),
                                torchlayers.Conv(128, groups = 16),
                                torchlayers.BatchNorm(),
                                torch.nn.Dropout(0.5),
                                torch.nn.ReLU(),  # use torch.nn wherever you wish
                                torchlayers.SqueezeExcitation(hidden=128),

                                torchlayers.GlobalMaxPool(),
                                torchlayers.Linear(self.classes)  # Output for 7 classes
                            )

                 #Full Model
        elif self.model_type == 'FULL':
            self.model = torch.nn.Sequential(
                        torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)

                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Residual(
                                torchlayers.Sequential(
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ReLU(),
                                    torchlayers.GroupNorm(num_groups=4),
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ChannelShuffle(groups=16),
                                    torchlayers.ReLU())),
                            torchlayers.BatchNorm(),
                            torch.nn.Dropout(0.5),
                            torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.5),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.SqueezeExcitation(hidden=128),
                           torchlayers.GlobalMaxPool(),
                          torchlayers.Linear(self.classes)  # Output for 7 classes
                            )

        elif self.model_type == 'FULL10':
            self.model = torch.nn.Sequential(
                        torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)

                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Residual(
                                torchlayers.Sequential(
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ReLU(),
                                    torchlayers.GroupNorm(num_groups=4),
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ChannelShuffle(groups=16),
                                    torchlayers.ReLU())),
                            torchlayers.BatchNorm(),
                            torch.nn.Dropout(0.5),
                            torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.5),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),

                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.SqueezeExcitation(hidden=128),
                           torchlayers.GlobalMaxPool(),
                          torchlayers.Linear(self.classes)  # Output for 7 classes
                            )

        elif self.model_type == 'FULL15':
            self.model = torch.nn.Sequential(
                        torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)

                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Residual(
                                torchlayers.Sequential(
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ReLU(),
                                    torchlayers.GroupNorm(num_groups=4),
                                    torchlayers.Conv(128, groups=16),
                                    torchlayers.ChannelShuffle(groups=16),
                                    torchlayers.ReLU())),
                            torchlayers.BatchNorm(),
                            torch.nn.Dropout(0.5),
                            torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.5),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.Residual(
                               torchlayers.Sequential(
                                   torchlayers.Conv(128, groups=16),
                                   torchlayers.ReLU(),
                                   torchlayers.GroupNorm(num_groups=4),
                                   torchlayers.Conv(128,  groups=16),
                                   torchlayers.ChannelShuffle(groups=16),
                                   torchlayers.ReLU())),
                           torchlayers.BatchNorm(),
                           torch.nn.Dropout(0.2),
                           torchlayers.SqueezeExcitation(hidden=128),
                           torchlayers.GlobalMaxPool(),
                          torchlayers.Linear(self.classes)  # Output for 7 classes
                            )


        elif self.model_type == "CNN_RNN":
                #self.model = torch.nn.Sequential(
                            #torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)

                        #torch.nn.LSTM(input_size = 11981, hidden_size = 128,  num_layers = 4, dropout = 0.4),

                        #torch.nn.Linear(127, self.classes))
            self.model = CNN_RNN_model(input_dim = self.input_dim,
                                           hidden_dim = self.hidden_dim,
                                           classes = self.classes)

        elif self.model_type == "RNN":
                #self.model = torch.nn.LSTM(128, num_layers=5, dropout = True)
            self.model = RNN_model(input_dim = self.input_dim,
                    hidden_dim = self.hidden_dim,
                    classes = self.classes)
        elif self.model_type == "ConvLSTM1d":
            pass
        elif self.model_type == "Autoencoder":
            self.model = AutoEncoder()
        elif self.model_type == "pretrained":


            self.model = pretrained(input_dim = self.input_dim,
                                        hidden_dim = self.hidden_dim,
                                        classes = self.classes,
                                        model_name = self.model_name
                                        )
        return self.model


class image_creator():
    def __init__():
        pass

    @staticmethod
    def convert_to_img(self, ds, classes):         #, logger):
            """


            """
            randomlist = []
            for i in range(0, 10):
                # any random numbers from 0 to 1000
                randomlist.append(random.randint(0, len(ds)))
            subset_images = randomlist
            subset = torch.utils.data.Subset(ds, subset_images)
            subset_dl = torch.utils.data.DataLoader(subset,
                                                        batch_size=1,
                                                        shuffle = False,
                                                        num_workers = 1)
            for x,y in subset_dl:
            #    print("i am here")
                sound = x[0].cpu().detach()
                label =  y.cpu().detach().numpy()
                self.plot_images(sound, label, classes)

    @staticmethod
    def plot_images(self, sound, label, classes):               #, logger):
        specgram = torchaudio.transforms.Spectrogram()(sound)

        fig, (ax0, ax1) = plt.subplots(nrows=2, constrained_layout=True)
        ax0.plot(sound.t().numpy())
        label = label[0]
        label_name = classes[label]
        ax0.set_title(f'Waveform of {label_name}')
        #self.logger[2].experiment.log_image(f"waveform and spec plot {label}")
        ax1.imshow(specgram.log2()[0,:,:].numpy(), cmap='Blues')
        ax1.set_title(f'Spectrogram of {label_name}')
        plt.show(block=False)
        #self.log('Waveform', fig)

        #logger[2].experiment.log_image('Waveform and Spectrogram', fig)

        plt.close()

class whaleDataModule(pl.LightningDataModule):
    def __init__(self, params, hparams):
        #, logger):
        super().__init__()
        #self.logger = logger
        self.params = params
        self.hparams = hparams
        self.batch_size = self.hparams["batch_size"]
        self.model_type = self.params["model_type"]
        self.size = self.params["size"]
        print("Setup Whale Data Module")
        self.classes = []


    def setup(self, stage = None):
        #transforms can be added here
        if stage == 'fit' or stage is None:
            start_tr = self.hparams['start_train']
            end_tr = self.hparams['end_train']
            print(f"train_folds: start -  {start_tr}, end - {end_tr}")
            train_folds = np.arange(self.hparams["start_train"], self.hparams['end_train'])
            #print(f"range of train_folds :{train_folds}")
            #train_folds = [self.hparams["start_train"], self.hparams['end_train']]
            self.train_ds = whale_dataset(self.params['csv_path'],
                                          self.params['file_path'],
                                          train_folds,
                                          model_type = self.model_type,
                                          size = self.size)
            val_folds = np.arange(self.hparams['start_val'], self.hparams['end_val'])

            #val_folds = [self.hparams['start_val'], self.hparams['end_val']]
            self.val_ds = whale_dataset(self.params['csv_path'],
                                        self.params['file_path'],
                                        val_folds,
                                        model_type = self.model_type,
                                        size = self.size)

        if stage == 'test' or stage is None:
            test_folds = np.array(self.hparams['test_folds'] )
            self.test_ds = whale_dataset (self.params['csv_path'],
                                          self.params['file_path'],
                                          test_folds,
                                          model_type = self.model_type,
                                          size = self.size)
        self.classes = self.train_ds.find_classes()



        print('------Data loaded-------')
        #Clear if needed
        #if self.model_type != 'pretrained':

        #    image_creator.convert_to_img(self.train_ds, self.classes#), self.logger[2])

    def train_dataloader(self):
        return DataLoader(self.train_ds,  self.batch_size, num_workers = 4, shuffle=True, pin_memory = True)

    def val_dataloader(self):
        return DataLoader(self.val_ds, self.batch_size, num_workers = 4, shuffle=False, pin_memory = True)

    def test_dataloader(self):
        return DataLoader(self.test_ds, self.batch_size, num_workers = 4, shuffle=False, pin_memory = True)



class whale_classifier(pl.LightningModule):


    def __init__(self, params, hyperparams):
        super().__init__()
        self.params = params
        self.hparams = hyperparams

        self.model_type = self.hparams["model_type"]     #Type of training simple, SE, RES etc.
        self.classes = self.hparams["classes"]
        self.y_accum = []
        self.y_hat_accum = []
        self.pred_tot = []
        self.y_tot = []
        self.input_dim = self.params["input_dim"]
        self.hidden_dim = self.params["hidden_dim"]
        self.model_name = self.params["model_name"]      #For pretrained model selection
        self.size = self.params["size"]

        self.model = self.model_select()
        #self.loss = nn.CrossEntropyLoss()
        #self.accuracy = Accuracy()


    def model_select(self):
        whale_net = whale_model_selector()
        self.model = whale_net.model_call(self.params, self.hparams)
        return self.model

    def forward(self, x):
        x = self.model(x)
        return x

    def configure_optimizers(self):
        # REQUIRED
        # can return multiple optimizers and learning_rate schedulers
        # (LBFGS it is automatically supported, no need for closure function)
        return torch.optim.Adam(self.parameters(), lr = self.hparams['lr'])

    def loss(self,logits, y):
        return F.cross_entropy(logits, y)

    def training_step(self, batch, batch_idx):
        # REQUIRED
        #x, y = batch
        #y_hat = self(x)

        #y_hat = y_hat.permute(1, 0)
        #loss = F.cross_entropy(y_hat, y)
        #self.log('training_loss', loss, prog_bar = True, logger = True)
        #pl.TrainResult(loss)
        #tensorboard_logs = {'train_loss': loss}
        ###ADD LOGGER HOOK
        #self.logger.experiment.

        #return {'train loss': train_loss, 'log': tensorboard_logs}

        ## Following the youtube video
        x, y = batch
        logits = self(x)
        train_loss = self.loss(logits, y)
        #pl.TrainResult(train_loss)

        preds = torch.argmax(logits, dim=1)
        self.log('train_loss', train_loss)
        #removed accuracy for now
        #self.log('accuracy', accuracy(preds,y))
        return train_loss

    def validation_step(self, batch, batch_idx):
        # OPTIONAL

        ## Taken from video
        #results['progress_bar']['val_acc'] = results['progress_bar']['train_acc']
        #del results['progress_bar']['train_acc']
        #return results

        x, y = batch
        logits = self(x)
        val_loss = self.loss(logits, y)
        preds = torch.argmax(logits, dim=1)
        #pl.TrainResult(train_loss)

        self.log('val_loss', val_loss)

        self.log('val_accuracy', accuracy(preds, y))


    def test_step(self, batch, batch_idx):
        # OPTIONAL

        self.prec = Precision(self.classes)
        self.rec = Recall(self.classes)
        #self.confmat = ConfusionMatrix(num_classes)
        self.f = F1(self.classes)
        x, y = batch
        logits = self(x)
        test_loss = self.loss(logits, y)
        y = y.detach().cpu()
        logits = logits.detach().cpu()
        preds = torch.argmax(logits, dim=1).detach().cpu()

        self.log('test_precision', self.prec(preds, y), on_epoch=True)
        self.log('test_recall', self.rec(preds, y), on_epoch=True)
        self.log('test_F1', self.f(preds, y), on_epoch=True)
        self.log('test_loss', test_loss, on_step=False, on_epoch=True)
        self.log('test_accuracy', accuracy(preds, y), on_epoch=True)
        self.log('conf_matrix', conf_matrix(preds, y, self.classes), on_step=False, on_epoch=True)

        #metrics = {'test_acc': ['val_acc'], 'test_loss': metrics['val_loss']}
        #self.log_dict(metrics)

    #    def test_epoch_end(self, outs):

    # log epoch metric
    #self.log('train_acc_epoch', self.accuracy.compute())
#        self.log('test_epoch_end/conf_mat', self.confmat.compute())
#        self.log('test_epoch_end/precision', self.precision.compute())
#        self.log('test_epoch_end/recall', self.recall.compute())
#        #self.log('test_epoch_end/accuracy', accuracy())
        # OPTIONAL
        #avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean()
        #logs = {'test_loss': avg_loss}
        #pred_tot = [x['prediction_accum'] for x in outputs]
        #y_tot = [x['y_accum'] for x in outputs]
#
#        self.log('test_loss', avg_loss, prog_bar = True, logger = True)
#        return {'avg_test_loss': avg_loss, 'log': logs, 'progress_bar': logs}


