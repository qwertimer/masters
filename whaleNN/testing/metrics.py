import pytorch_lightning as pl
import matplotlib.pyplot as plt

#plotting
from sklearn.metrics import plot_confusion_matrix as plt_cm
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from scikitplot.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score
import numpy as np

class metrics_logging():
    @staticmethod
    def f_score(self, y_true, y_pred, neptune_logger):
        f_score = f1_score(y_true, y_pred)
        print(f_score)
        neptune_logger.experiment.log_metric('f_score', f_score)
        accuracy = accuracy_score(y_true, y_pred)
        neptune_logger.experiment.log_metric('test_accuracy', accuracy)
    @staticmethod
    def conf_matrix(self, y_true, y_pred, neptune_logger):
        # Log charts
        fig, ax = plt.subplots(figsize=(16, 12))
        #plot_confusion_matrix(y_true, y_pred, ax=ax, labels=class_names)
        cm = confusion_matrix(y_true, y_pred)
        cm_display = ConfusionMatrixDisplay(cm, display_labels=class_names)

        disp = cm_display.plot(
                     cmap='Blues', ax=ax)
        plt.xticks(rotation=45)
        plt.show()

        #plt_cm(model, y_pred, y_true, display_labels = class_names, cmap = plt.cm.Blues)
        neptune_logger.experiment.log_image('confusion_matrix', fig)



