#!/bin/bash

#train against VGG13
echo 'training against vgg13'
python __main__.py --config_file=vgg_13.json --experiment_name=vgg_13
#train against VGG16
echo 'training against vgg16'
python __main__.py --config_file=vgg_16.json --experiment_name=vgg_16
#train against VGG19 
echo 'training against vgg19'
python __main__.py --config_file=vgg_19.json --experiment_name=vgg_19
#train against resnet34 
echo 'training against resnet34'
python __main__.py --config_file=resnet34.json --experiment_name=resnet34
#train against resnet50 
echo 'training against resnet50'
python __main__.py --config_file=resnet50.json --experiment_name=resnet50
#train against resnet101 
echo 'training against resnet101'
python __main__.py --config_file=resnet101.json --experiment_name=resnet101
#train against squeezenet 
echo 'training against squeezenet'
python __main__.py --config_file=squeezenet.json --experiment_name=squeezenet
#train against densenet 
echo 'training against densenet'
python __main__.py --config_file=densenet121.json --experiment_name=densenet121






