#!/bin/sh

tmux new-session -s whale_NN -n Main -d
tmux new-window -t whale_NN -n Code 
tmux new-window -t whale_NN -n Dev
tmux new-window -t whale_NN -n Notes


#Main window with console script and terminal
tmux select-window -t whale_NN:Main
tmux send-keys -t whale_NN:Main "vi ~/Documents/Masters/2020\ Code/Production/Lightning_whale/whaleNN/__main__.py" Enter
tmux split-window -v

#Documentation Pane
tmux send-keys -t whale_NN:Notes "vi ~/vimwiki/index.wiki" Enter

#Code Pane
tmux select-window -t whale_NN:Code
tmux send-keys -t whale_NN:Code "vi ~/Documents/Masters/2020\ Code/Production/Lightning_whale/whaleNN/training/lightning_build.py" Enter
tmux split-window -v
tmux send-keys -t whale_NN:Code "vi ~/Documents/Masters/2020\ Code/Production/Lightning_whale/whaleNN/dataset/dataset.py" Enter
tmux split-window -h
tmux send-keys -t whale_NN:Code "vi ~/Documents/Masters/2020\ Code/Production/Lightning_whale/whaleNN/models/models.py" Enter
tmux select-layout tiled

#Introductory Code
tmux select-window -t whale_NN:Dev
tmux send-keys -t whale_NN:Dev "vi ~/Documents/Masters/2020\ Code/Production/Lightning_whale/whaleNN/traditional_approach/traditional_classifiers.py" Enter
tmux split-window -v

#start at Main
tmux select-window -t whale_NN:Main
tmux -u attach -t whale_NN

