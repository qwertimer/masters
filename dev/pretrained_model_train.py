import pdb
import torch
import pytorch_lightning as pl
import torchlayers
import torch.nn as nn
import torch.functional as f
import torchvision.models as models
from pathlib import Path
import json
from easydict import EasyDict as edict
class CNN_model(pl.LightningModule):
    def __init__(self ):
        super().__init__()

        config_file = "config.json"
        params, device = self.set_params(config_file)


        file_path = params.LOCATIONS.file_path
        ROOT = params.LOCATIONS.ROOT
        project = params.LOCATIONS.project
        kfold_manifest = params.LOGGING.kfold_manifest
        exp_path = ROOT + project
        csv_path = exp_path + '/' + kfold_manifest

        #HyperParams from json 
        self.params = params
        #self.classes = self.hparams["classes"]
        self.classes = params.HYPERPARAMETERS.n_classes
        #self.num_target_classes = len(self.classes)
        ##
        ##
        #self.feature_extractor = self.models_pretrained()
        ##self.feature_extractor.eval()

        # use the pretrained model to classify cifar-10 (10 image classes)
        self.classifier = nn.Linear(2048, self.classes)



    def forward(self, x):
        representations = self.feature_extractor(x)
        x = self.classifier(representations)

    def models_pretrained(self):
         model_list = ["alexnet",
                "vgg11",
                "vgg13",
                "vgg16",
                "vgg19",
                "vgg11_bn",
                "vgg13_bn",
                "vgg16_bn",
                "vgg19_bn",
                "resnet18",
                "resnet34",
                "resnet50",
                "resnet101",
                "resnet152",
                "squeezenet1_0",
                "squeezenet1_1",
                "densenet121",
                "densenet169",
                "densenet201",
                "densenet161",
                "inception_v3",
                "googlenet",
                "shufflenet_v2_x1_0",
                "mobilenet_v2",
                "resnext50-32x4d",
                "resnext101_32x8d",
                "wide_resnet50_2",
                "wide_resnet101_2",
                "mnasnet1_0"
                ]
        print("Select Model")
        dictionary = {}
        num_columns = 2
        for i, model in enumerate(model_list):
            strp = (f"{i} : {model}")
            print(strp)
            i = str(i)
            dictionary.update({i : model})
        index = input()
        model_list_item = dictionary.get(index)
        net = getattr(models,model_list_item)(pretrained = True)

        return net

    def set_params(self, config_file = "config.json"):
         """
         set_params: The set params function pulls in the  json file c
         Aswell the function gets the hyperparams and parameters that
         args:
             config_file : json file containing the attributes

         returns:
             params : all parameters as a dictionary
             default_device : CUDA check
         """
         exp_path = Path.cwd()
         torch.cuda.empty_cache()

         # Load Test Parameters
         with open(str(exp_path) + '/training/' + str(config_file), "r") as f:    
             x = json.load(f)
         params = edict(x)
         default_device = torch.device(
             'cuda' if torch.cuda.is_available() else 'cpu')
     
         return params, default_device






def main():
    network = CNN_model()
    new_model = network.models_pretrained()
    print(new_model)

if __name__ == "__main__":
    main()
