
from tqdm import tqdm_notebook as tqdm
from collections import defaultdict, OrderedDict

import os
import sys
from pathlib import Path
import csv
import torch
import librosa


####_______________________________________________####


"""
This code block provides all the functions for getting files
based extension type.

"""
import mimetypes
from typing import *
import pandas as pd

def listify(o):
    if o is None: return []
    if isinstance(o, list): return o
    if isinstance(o, str): return [o]
    if isinstance(o, Iterable): return list(o)
    return [o]

#export
def setify(o): return o if isinstance(o,set) else set(listify(o))


def get_extensions(type = 'audio'):
    if type =='audio':
        extensions = tuple(str.lower(k) for k, v in mimetypes.types_map.items() if v.startswith('audio/'))
    else:
        extensions = None
    return extensions

def _get_files(p, fs, extensions=None):
    p = Path(p)
    res = [p/f for f in fs if not f.startswith('.')
        and ((not extensions) or f'.{f.split(".")[-1].lower()}' in extensions)]
    return res


def get_files(path, extensions=None, recurse=False, include=None):
    path = Path(path)

    extensions = setify(extensions)
    extensions = {e.lower() for e in extensions}
    if recurse:
        res = []
        for i,(p,d,f) in enumerate(os.walk(path)): # returns (dirpath, dirnames, filenames)

            if include is not None and i==0: d[:] = [o for o in d if o in include]
            else:                            d[:] = [o for o in d if not o.startswith('.')]
            res += _get_files(p, f, extensions)
        return res
    else:
        f = [o.name for o in os.scandir(path) if o.is_file()]
        return _get_files(path, f, extensions)

def build_manifest(ROOT, audio_extensions):
    filenames = []
    labels = []
    folders = []
    files = get_files(ROOT, audio_extensions, recurse = True)
    for file in files:
        if not file.stem.endswith('noise'):
            #print(file.stem)
            filenames.append((file.stem))

            labels.append(file.parts[-2])
            folders.append(str(file.parents[1]))

    return filenames, labels, folders


def manifest_to_csv(ROOT, audio_extensions, csv_path):

    ##Initial manifest. uncomment if files need to be cleaned
    filenames, labels, folders  = build_manifest(ROOT, audio_extensions)
    df = pd.DataFrame((pd.DataFrame(data = [filenames, labels, folders]).T))
    df.columns = ['filenames', 'labels', 'folders']
    df.to_csv(csv_path, sep = ',', index = False)
    manifest_data = filenames, labels, folders
    return manifest_data, df

####_________________________________________________####

"""
This code block provides the functions to create a kfold
splitter for cross-validation
"""
import sklearn
from sklearn.model_selection import StratifiedKFold as KFold
import numpy as np
import shutil


folds = 10


def KFold_Splitter(folds=5, seed=None, **kwargs):
    "Create function that splits `items` into n folds."
    def _inner(o, **kwargs):
        if seed is not None: torch.manual_seed(seed)
        rand_idx = np.asarray(list(int(i) for i in torch.randperm(len(o))))
        percent = int(len(o)/folds)
        data_splits = np.array_split(rand_idx, folds)
        return data_splits
    return _inner

def build_fold_folders(loc, folds = 10):
    folders = []
    for val in range(folds):


        folder = str(loc) + 'Fold_' + str(val)
        if not os.path.exists(folder):
            os.mkdir(folder)
        folders.append(folder)
    return folders




def build_folds(folders, splits, df, params):
    base = Path(folders[0])
    base = str(base.parents[0])
    base = params.LOCATIONS.file_path
    for fold, split in enumerate(splits):
        fold_label = 'Fold_' + str(fold)
        data = df.loc[split]
        for index, row in data.iterrows():
            filename = row[0]
            species = row[1]
            folder = row[2]
            item = folder + '/' + species + '/' + filename

            if not item.endswith("noise"):
                item = item +'.wav'
                shutil.copy(item, (base + '/' + fold_label + '/'+ filename +'.wav'))

    return(print('folds created'))

def process_folds(loc, folds, manifest_data):
    """
    function to create fold folders in
    """
    #When fold folders need to be rebuilt
    #loc = '/run/media/qwertimer/SSD/Masters/Datasets/Master Whale Sounds/Master Whale Sounds/kFold/'
    filenames, _ = manifest_data
    folders = build_fold_folders(loc, folds)
    res = KFold_Splitter()
    splits = res(filenames)
        ##Fold builder
    build_folds(folders, splits, df)

####_________________________________________________####
#Build csv
def csv_info(res, fn = 'audio.csv'):
    with open(fn, mode='w') as file:
        file_writer = csv.writer(file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

        file_writer.writerow(["filenames", "skip", "start", "end", "salience", "folders", "class_ID_one_hot", "labels"])
        for filenames, folders, class_ID_one_hot, labels in res:
             file_writer.writerow([filenames, 0, 0, 0, 0, folders, class_ID_one_hot, labels])
    return file





####_________________________________________________####
#Experimental setup Codes
from sklearn import preprocessing

def get_label(file):
    """
    gets labelname from files

    """

    res = ""
    file = str(file)

    val = file.split(os.path.sep)[-1] #Removes parent folders
    val = Path(val).stem              #Removes extension
    for item in val:
        if item.isalpha() == True:

            res += (item)
    label = (res)
    return label


def get_ClassID(files):
    le_item = []
    for file in files:
        name = get_label(file)
        le_item.append(name)
    le = preprocessing.LabelEncoder()
    le.fit(le_item)
    classes = list(le.classes_)
    one_hot = le.transform(le_item)
    class_dict = dict(zip(le_item, one_hot))
    return class_dict

def build_train_manifest(ROOT, audio_extensions):
    filenames = []
    labels = []
    folders = []
    class_ID_one_hot = []
    res = []


    files = get_files(ROOT,
                      audio_extensions,
                      recurse = True
                      )

    print("get ClassIDs")
    class_dict = get_ClassID(files)

    print("build Manifest")

    for file in files:

        name = get_label(file)
        #print(name)

        if name in class_dict.keys():
            classID = class_dict[name]
            class_ID_one_hot.append(classID)        #6 classID
            #print(file.stem)
        filenames.append((file.stem + '.wav'))   #0      ##Filename U+2713
            #1 skip
            #2 start
            #3 end
            #4 Salience
        fold = str(file.parts[-2])
        item = fold.split('_')[1]
        folders.append(item)   #5 fold

        #6 classID
        labels.append(name)          #7 label
        res = zip(filenames, folders, class_ID_one_hot, labels)
    return res









####_________________________________________________####
#Acoustic Signal Processing
import scipy.signal as sps

def sample_normalization(sound, new_rate):

    audio = sound[0]
    sr = sound[1]
    audio_np = np.asarray(audio)[0]
    audio_val = audio_np.shape[0]
    audio_np = np.asfortranarray(audio_np)

    #print(f'lenaudio = {len(audio_np)}')
    y_hat = librosa.core.resample(audio_np, sr, new_rate, res_type='kaiser_best', fix=True, scale=False)

    #number_of_samples = round((audio_val) * float(new_rate) / sr)
    #print(f'no of samples = {number_of_samples}')

    #audio = sps.resample(audio, number_of_samples)

    sound = (y_hat, new_rate)
    return(sound)
