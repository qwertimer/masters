import torch
import torch.nn as nn
import torch.nn.functional as F
import torchlayers as tl
import pytorch_lightning
import torchaudio
from torchvision import models
class CNN_RNN_model(nn.Module):
    def __init__(self, input_dim, hidden_dim, classes):
        super(CNN_RNN_model, self).__init__()
        self.hidden_dim = hidden_dim
        self.input_dim = input_dim
        self.classes = classes
        self.cnn = nn.Conv1d(1, 128, kernel_size = 80, stride = 4)

        self.lstm = nn.LSTM(self.hidden_dim, self.hidden_dim, num_layers = 5, dropout = 0.4)
        self.fc = nn.Linear(self.hidden_dim,self.classes)

    def forward(self, x):
        x = self.cnn(x)
        x, _ =  self.lstm(x)
        x = self.fc(x[:, -1, :])
        return(F.log_softmax(x, dim=1))

class RNN_model(nn.Module):

    def __init__(self, input_dim, hidden_dim, classes):
        super(RNN_model, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.classes = classes
        self.lstm = nn.LSTM(self.input_dim,self.hidden_dim, num_layers = 5, dropout = 0.4)
        self.fc = nn.Linear(self.hidden_dim, self.classes)

    def forward(self, x):
        x, _ = self.lstm(x)
        x = self.fc(x[:, -1, :])
        return(F.log_softmax(x, dim=1))


class AutoEncoder(torch.nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()
        self.encoder = tl.Sequential(
            tl.StandardNormalNoise(),  # Apply noise to input images
            torch.nn.Conv1d(1, 128,80,4),
            tl.activations.Swish(),  # Direct access to module .activations
            tl.InvertedResidualBottleneck(squeeze_excitation=False),
            tl.AvgPool(),  # shape 64 x 128 x 128, kernel_size=2 by default
            tl.HardSwish(),  # Access simply through tl
            tl.SeparableConv(128),  # Up number of channels to 128
            tl.InvertedResidualBottleneck(),  # Default with squeeze excitation
            torch.nn.ReLU(),
            tl.AvgPool(),  # shape 128 x 64 x 64, kernel_size=2 by default
            tl.DepthwiseConv(256),  # DepthwiseConv easier to use
            # Pass input thrice through the same weights like in PolyNet
            tl.Poly(tl.InvertedResidualBottleneck(), order=3),
            tl.ReLU(),  # all torch.nn can be accessed via tl
            tl.MaxPool(),  # shape 256 x 32 x 32
            tl.Fire(out_channels=512),  # shape 512 x 32 x 32
            tl.SqueezeExcitation(hidden=64),
            tl.InvertedResidualBottleneck(),
            tl.MaxPool(),  # shape 512 x 16 x 16
            tl.InvertedResidualBottleneck(squeeze_excitation=False),
            # Randomly switch off the last two layers with 0.5 probability
            tl.StochasticDepth(
                torch.nn.Sequential(
                    tl.InvertedResidualBottleneck(squeeze_excitation=False),
                    tl.InvertedResidualBottleneck(squeeze_excitation=False),
                ),
                p=0.5,
            ),
            tl.AvgPool(),  # shape 512 x 8 x 8
        )

        # This one is more "standard"
        self.decoder = tl.Sequential(
            tl.Poly(tl.InvertedResidualBottleneck(), order=2),
            # Has ICNR initialization by default after calling `build`
            tl.ConvPixelShuffle(out_channels=512, upscale_factor=2),
            # Shape 512 x 16 x 16 after PixelShuffle
            tl.Poly(tl.InvertedResidualBottleneck(), order=3),
            tl.ConvPixelShuffle(out_channels=256, upscale_factor=2),
            # Shape 256 x 32 x 32
            tl.Poly(tl.InvertedResidualBottleneck(), order=3),
            tl.ConvPixelShuffle(out_channels=128, upscale_factor=2),
            # Shape 128 x 64 x 64
            tl.Poly(tl.InvertedResidualBottleneck(), order=4),
            tl.ConvPixelShuffle(out_channels=64, upscale_factor=2),
            # Shape 64 x 128 x 128
            tl.InvertedResidualBottleneck(),
            tl.Conv(256),
            tl.Dropout(),  # Defaults to 0.5 and Dropout2d for images
            tl.Swish(),
            tl.InstanceNorm(),
            tl.ConvPixelShuffle(out_channels=32, upscale_factor=2),
            # Shape 32 x 256 x 256
            tl.Conv(16),
            tl.Swish(),
            torch.nn.Conv1d(1, 128,80,4)
            # Shape 3 x 256 x 256
        )

    def forward(self, inputs):
        return self.decoder(self.encoder(inputs))


class CNN_Spectrogram(nn.Module):
    def __init__():
        self.spec = torchaudio.transforms.Spectrogram()(x)
        self.model = torch.nn.Sequential(self.spec,)  ## To do build a Residual model here. 


def forward():
    return self.model(x)


class pretrained(nn.Module):
    def __init__(self, input_dim, hidden_dim, classes):
        super(pretrained, self).__init__()
        self.feature_extractor = self.model_selector()
        self.hidden_dim = hidden_dim
        self.classifier = nn.Linear(hidden_dim, classes)
        self.feature_extractor.eval()

    def forward(self, x):
        representations = self.feature_extractor(x)
        x = self.classifier(representations)

    def model_selector(self):
        model_list = ["alexnet",
                "vgg11",
                "vgg13",
                "vgg16",
                "vgg19",
                "vgg11_bn",
                "vgg13_bn",
                "vgg16_bn",
                "vgg19_bn",
                "resnet18",
                "resnet34",
                "resnet50",
                "resnet101",
                "resnet152",
                "squeezenet1_0",
                "squeezenet1_1",
                "densenet121",
                "densenet169",
                "densenet201",
                "densenet161",
                "inception_v3",
                "googlenet",
                "shufflenet_v2_x1_0",
                "mobilenet_v2",
                "resnext50-32x4d",
                "resnext101_32x8d",
                "wide_resnet50_2",
                "wide_resnet101_2",
                "mnasnet1_0"
                ]
        print("Select Model")
        dictionary = {}
        num_columns = 2
        for i, model in enumerate(model_list):
            strp = (f"{i} : {model}")
            print(strp)
            i = str(i)
            dictionary.update({i : model})
        index = input()
        model_list_item = dictionary.get(index)
        net = getattr(models,model_list_item)(pretrained = True)

        return net
