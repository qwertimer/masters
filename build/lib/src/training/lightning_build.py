import os
import random
import matplotlib.pyplot as plt
import numpy as np
#Pytorch Basics
import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
#from torchvision import transforms
#Audio
import librosa
#import librosa.display 
import torchaudio
from scipy import signal

import pytorch_lightning as pl

import torchlayers

from dataset.dataset import whale_dataset
from scikitplot.metrics import plot_confusion_matrix
import torch.nn as nn

#personal
from ..models.models import CNN_RNN_model, RNN_model, AutoEncoder, pretrained
from ..testing.metrics import conf_matrix, f_score

class whale_classifier(pl.LightningModule):

    def __init__(self, params, hyperparams):
        super(whale_classifier, self).__init__()
        self.params = params
        self.hparams = hyperparams

        self.model_type = self.hparams["model_type"]
        self.classes = self.hparams["classes"]
        self.y_accum = []
        self.y_hat_accum = []
        self.pred_tot = []
        self.y_tot = []


        self.input_dim = self.params["input_dims"]
        self.hidden_dim = self.params["hidden_dim"]

        if self.model_type == 'SIMPLE':

            self.model = torch.nn.Sequential(
                        torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                        torchlayers.BatchNorm(),
                        torch.nn.ReLU(),  # use torch.nn wherever you wish
                        torchlayers.Conv(128, groups = 16),
                        torchlayers.BatchNorm(),
                        torch.nn.ReLU(),  # use torch.nn wherever you wish
                        torchlayers.Conv(128, groups = 16),
                        torchlayers.BatchNorm(),
                        torch.nn.ReLU(),  # use torch.nn wherever you wish
                        torchlayers.Conv(128, groups = 16),
                        torchlayers.BatchNorm(),
                        torchlayers.ReLU(),
                        torchlayers.Conv(128, groups = 16),
                        torchlayers.BatchNorm(),

                      torchlayers.GlobalMaxPool(),
                      torchlayers.Linear(self.classes)  # Output for 7 classes
                        )
          #Model with Residual Blocks
        elif self.model_type == 'RES':

            self.model = torch.nn.Sequential(
                        torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                        torchlayers.BatchNorm(),
                        torch.nn.ReLU(),  # use torch.nn wherever you wish
                        torchlayers.Residual(
                            torchlayers.Sequential(
                                torchlayers.Conv(128, groups=16),
                                torchlayers.ReLU(),
                                torchlayers.GroupNorm(num_groups=4),
                                torchlayers.Conv(128, groups=16),
                                torchlayers.ChannelShuffle(groups=16),
                                torchlayers.ReLU())),
                       torchlayers.BatchNorm(),
                       torchlayers.Residual(
                           torchlayers.Sequential(
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ReLU(),
                               torchlayers.GroupNorm(num_groups=4),
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ChannelShuffle(groups=16),
                               torchlayers.ReLU())),
                       torchlayers.BatchNorm(),
                       torchlayers.Residual(
                           torchlayers.Sequential(
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ReLU(),
                               torchlayers.GroupNorm(num_groups=4),
                               torchlayers.Conv(128,  groups=16),
                               torchlayers.ChannelShuffle(groups=16),
                               torchlayers.ReLU())),
                       torchlayers.BatchNorm(),
                       torchlayers.Residual(
                           torchlayers.Sequential(
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ReLU(),
                               torchlayers.GroupNorm(num_groups=4),
                               torchlayers.Conv(128,  groups=16),
                               torchlayers.ChannelShuffle(groups=16),
                               torchlayers.ReLU())),
                       torchlayers.BatchNorm(),
                      torchlayers.GlobalMaxPool(),
                      torchlayers.Linear(self.classes)  # Output for 7 classes
                        )

              #Model with Squeeze and Excitation
        elif self.model_type == 'SE':
            self.model = torch.nn.Sequential(
                            torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.SqueezeExcitation(hidden=128),
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Conv(128, groups = 16),
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.Conv(128, groups = 16),
                            torchlayers.BatchNorm(),
                            torch.nn.ReLU(),  # use torch.nn wherever you wish
                            torchlayers.SqueezeExcitation(hidden=128),

                            torchlayers.GlobalMaxPool(),
                            torchlayers.Linear(self.classes)  # Output for 7 classes
                        )

             #Full Model
        elif self.model_type == 'FULL':
            self.model = torch.nn.Sequential(
                    torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)
                        torchlayers.BatchNorm(),
                        torch.nn.ReLU(),  # use torch.nn wherever you wish
                        torchlayers.Residual(
                            torchlayers.Sequential(
                                torchlayers.Conv(128, groups=16),
                                torchlayers.ReLU(),
                                torchlayers.GroupNorm(num_groups=4),
                                torchlayers.Conv(128, groups=16),
                                torchlayers.ChannelShuffle(groups=16),
                                torchlayers.ReLU())),
                        torchlayers.BatchNorm(),
                        torchlayers.Residual(
                           torchlayers.Sequential(
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ReLU(),
                               torchlayers.GroupNorm(num_groups=4),
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ChannelShuffle(groups=16),
                               torchlayers.ReLU())),
                       torchlayers.BatchNorm(),
                       torchlayers.Residual(
                           torchlayers.Sequential(
                               torchlayers.Conv(128, groups=16),
                               torchlayers.ReLU(),
                               torchlayers.GroupNorm(num_groups=4),
                               torchlayers.Conv(128,  groups=16),
                               torchlayers.ChannelShuffle(groups=16),
                               torchlayers.ReLU())),
                       torchlayers.BatchNorm(),
                       torchlayers.SqueezeExcitation(hidden=128),
                       torchlayers.GlobalMaxPool(),
                      torchlayers.Linear(self.classes)  # Output for 7 classes
                        )

        elif self.model_type == "CNN_RNN":
            #self.model = torch.nn.Sequential(
                        #torch.nn.Conv1d(1, 128, 80, 4),  #In channels and receptive field (large field for audio capture)

                    #torch.nn.LSTM(input_size = 11981, hidden_size = 128,  num_layers = 4, dropout = 0.4),

                    #torch.nn.Linear(127, self.classes))
            self.model = CNN_RNN_model(input_dim = self.input_dim, 
                                       hidden_dim = self.hidden_dim,
                                       classes = self.classes)

        elif self.model_type == "RNN":
            #self.model = torch.nn.LSTM(128, num_layers=5, dropout = True)
            self.model = RNN_model(input_dim = self.input_dim, 
                    hidden_dim = self.hidden_dim,
                    classes = self.classes)
        elif self.model_type == "ConvLSTM1d":
            pass
        elif self.model_type == "Autoencoder":
            self.model = AutoEncoder()
        elif self.model_type == "pretrained":


            self.model = pretrained(input_dim = self.input_dim,
                                    hidden_dim = self.hidden_dim,
                                    classes = self.classes)


    def prepare_data(self):

        train_folds = range(self.hparams["start_train"], self.hparams['end_train'])
        self.train_ds = whale_dataset(self.params['csv_path'],
                                  self.params['file_path'],
                                  train_folds,
                                  model_type = self.model_type)
        self.classes = self.train_ds.find_classes()
        val_folds = range(self.hparams['start_val'], self.hparams['end_val'])
        self.val_ds = whale_dataset(self.params['csv_path'],
                                    self.params['file_path'],
                                    val_folds,
                                    model_type = self.model_type)
        self.test_ds = whale_dataset (self.params['csv_path'],
                                      self.params['file_path'],
                                      self.hparams['test_folds'],
                                      model_type = self.model_type)


        print('------Data loaded-------')
        ##build a subset of training images for viewing and saving into the report
        ##pseudocode
        #build plot dataset from trainds
        #Get sound, sr, and label
        #def plot image
        randomlist = []

        if self.model_type != 'pretrained':
            convert_to_img()
        def convert_to_img():
            for i in range(0, 10):
            # any random numbers from 0 to 1000
                 randomlist.append(random.randint(0, len(self.train_ds)))
                 print(randomlist)
            subset_images = randomlist
            subset = torch.utils.data.Subset(self.train_ds, subset_images)
            subset_dl = torch.utils.data.DataLoader(subset,
                                                    batch_size=1,
                                                    shuffle = False,
                                                    num_workers = 1)
            for x,y in subset_dl:
            #    print("i am here")
                 sound = x[0].cpu().detach()
                 label =  y.cpu().detach().numpy()
                 self.plot_images(sound, label, self.classes)

    def plot_images(self, sound, label, classes):
        #plt.subplot(211)
        #plt.plot(sound.t().numpy()) 
        #Spectrogram
        specgram = torchaudio.transforms.Spectrogram()(sound)

        #print("Shape of spectrogram: {}".format(specgram.size()))
        #plt.subplot(212)
        #plt.imshow(specgram.log2()[0,:,:].numpy(), cmap='blues')
        #plt.show()


        #####TEST

        fig, (ax0, ax1) = plt.subplots(nrows=2, constrained_layout=True)
        ax0.plot(sound.t().numpy())
        label = label[0]
        label_name = classes[label]
        ax0.set_title(f'Waveform of {label_name}')
        #self.logger[2].experiment.log_image(f"waveform and spec plot {label}")
        ax1.imshow(specgram.log2()[0,:,:].numpy(), cmap='Blues')
        ax1.set_title(f'Spectrogram of {label_name}')
        plt.show()
        self.logger[2].experiment.log_image('Waveform and Spectrogram', fig)
        plt.close()

    def forward(self, x):

        return self.model(x)


    def training_step(self, batch, batch_nb):
        # REQUIRED
        x, y = batch
        y_hat = self.forward(x)

        #y_hat = y_hat.permute(1, 0)
        loss = F.cross_entropy(y_hat, y)

        tensorboard_logs = {'train_loss': loss}

        ###ADD LOGGER HOOK
        #self.logger.experiment.


        return {'loss': loss, 'log': tensorboard_logs}

    def validation_step(self, batch, batch_nb):
        # OPTIONAL
        
        x, y = batch
        y_hat = self.forward(x)

        return {'val_loss': F.cross_entropy(y_hat, y)}

    def validation_epoch_end(self, outputs):
        # OPTIONAL
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        tensorboard_logs = {'val_loss': avg_loss}
        return {'avg_val_loss': avg_loss, 'log': tensorboard_logs}

    def test_step(self, batch, batch_nb):
        # OPTIONAL
        x, y = batch
        y_hat = self(x)

        # Accum summaries for confusion matrix
        self.y_accum.append(y)
        self.y_hat_accum.append(y_hat)
        return {'test_loss': F.cross_entropy(y_hat, y), 'prediction_accum' : self.y_hat_accum, 'y_accum' : self.y_accum}

    def test_epoch_end(self, outputs):
        # OPTIONAL
        avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean()
        logs = {'test_loss': avg_loss}
        self.pred_tot = [x['prediction_accum'] for x in outputs]
        self.y_tot = [x['y_accum'] for x in outputs]

        #Below will be used to plot the first layer weights
        #conv1 = self.model.modules()
        #conv1_params = conv1.named_parameters
        #print(conv1_params)
        return {'avg_test_loss': avg_loss, 'log': logs, 'progress_bar': logs}

    def configure_optimizers(self):
        # REQUIRED
        # can return multiple optimizers and learning_rate schedulers
        # (LBFGS it is automatically supported, no need for closure function)
        return torch.optim.Adam(self.parameters(), lr = self.hparams['lr'])

    def train_dataloader(self):
        return DataLoader(self.train_ds,  batch_size=32, num_workers = 4, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.val_ds, batch_size=32, num_workers = 4, shuffle=False)

    def test_dataloader(self):
        return DataLoader(self.test_ds, batch_size=16, num_workers = 4, shuffle=False)
