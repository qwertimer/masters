from utils.utils import sample_normalization
from torch.utils.data import Dataset
import torch
import torchaudio
import librosa
import pandas as pd

import itertools


class whale_dataset(Dataset):

    def __init__(self, csv_path, file_path, folderList, model_type = 'other'):

        csvData = pd.read_csv(csv_path)

        #initialize lists to hold file names, labels, and folder numbers
        self.file_names = []
        self.labels = []
        self.folders = []
        self.label_names = []

        self.model_type = model_type 
        #loop through the csv entries and only add entries from folders in the folder list
        for i in range(0,len(csvData)):

            if csvData.iloc[i, 5] in folderList:

                self.file_names.append(csvData.iloc[i, 0])

                self.labels.append(csvData.iloc[i, 6])
                self.folders.append(csvData.iloc[i, 5])

                self.label_names.append(csvData.iloc[i, 7])

        self.file_path = file_path
        #self.mixer = torchaudio.transforms.DownmixMono() #UrbanSound8K uses two channels, this will convert them to one
        self.folderList = folderList

        if self.model_type == 'pretrained':
        #Additional_flags
            self.log_mel = False
            self.spectrogram =True 
            print("Convert to spectrogram")

        #self.count


    def __getitem__(self, index):
        #format the file path and load the file
        new_sample_rate = 16000
        path = self.file_path + "Fold_" + str(self.folders[index]) + "/" + self.file_names[index]

        try:
            self.sound, self.sr = torchaudio.load(path, out = None, normalization = True)

        except RuntimeError:

            self.sound = torch.zeros(new_sample_rate*3)
            self.sound = [self.sound, new_sample_rate]
            print(f"error = {path}")

        self.sound = torchaudio.transforms.Resample(self.sr, new_sample_rate)(self.sound)
        tempData = torch.zeros([48000])
        if (len(self.sound[0])) < 48000:
            tempData[:len(self.sound[0])] = self.sound

        else:
            tempData[:] = self.sound[:48000]
        self.sound = tempData
        self.sound = self.sound.unsqueeze(0)
        if self.spectrogram == True:
            self.sound = torchaudio.transforms.Spectrogram()(self.sound) 
        return self.sound, self.labels[index]     #soundFormatted changed


    def __len__(self):
        return len(self.file_names)


    def find_classes(self):
        class_labels = self.labels
        class_idx = self.label_names
        class_dict = dict(zip(class_labels, class_idx))
        #class_dict = {class_labels : class_idx}
        return class_dict



    def precompute_spectrograms(path, dpi=50):
        files = Path(path).glob('*.wav')
        for filename in files:
            audio_tensor, sample_rate = librosa.load(filename, sr=None)
            spectrogram = librosa.feature.melspectrogram(audio_tensor, sr=sr)
            log_spectrogram = librosa.power_to_db(spectrogram, ref=np.max)
            librosa.display.specshow(log_spectrogram, sr=sr, x_axis='time',
                                     y_axis='mel')
            plt.gcf().savefig("{}{}_{}.png".format(filename.parent,dpi,
                              filename.name),dpi=dpi)




