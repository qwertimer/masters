#system imports
import os
from pathlib import Path
from dotenv import load_dotenv
#dictionary imports
import json
from easydict import EasyDict as edict

#nn imports
import torch
import pytorch_lightning as pl
from pytorch_lightning.loggers import CometLogger, TensorBoardLogger, NeptuneLogger
from pytorch_lightning.callbacks import ModelCheckpoint

#plotting
from sklearn.metrics import plot_confusion_matrix as plt_cm
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from scikitplot.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score
import numpy as np


#wrapper for torch models
import torchlayers

#personal
from training.lightning_build import whale_classifier as wc



def set_params(config_file = "/config/config.json"):
    """
    set_params: The set params function pulls in the  json file containing the absolute path locations of all the required files.
    Aswell the function gets the hyperparams and parameters that aare needed for training.
    args:
        config_file : json file containing the attributes

    returns:
        params : all parameters as a dictionary
        default_device : CUDA check
    """
    exp_path = Path.cwd()
    torch.cuda.empty_cache()

    # Load Test Parameters
    with open(str(exp_path) + '/training/config' + str(config_file), "r") as f:
        x = json.load(f)
    params = edict(x)
    default_device = torch.device(
        'cuda' if torch.cuda.is_available() else 'cpu')

    return params, default_device

def build_experiment_folders():
    """
    Allows user to input an experiment name and appends to the current working directory an experiment folder
    if it doesnt exist and then places a log and checkpoint directory inside.

    returns:
        loggers : logger folder
        chckpoints : checkpoint folder
        experiment_name : name of experiment
    """
    x = True
    print(' Please enter experiment name =')
    cwd = os.getcwd()
    experiments = os.path.join(cwd, 'experiments')
    if not os.path.exists(experiments):
        os.mkdir(experiments)

    experiment_name = input()
    while(x == True):

        exp_dir = os.path.join(experiments, experiment_name)

        if not os.path.exists(exp_dir):
            print(exp_dir)
            os.makedirs(exp_dir)
            x = False
        else:
            print('Experiment Name exists, Do you want to overwrite it? (yes, no)')
            ans = input()
            if ans == 'yes':
                x = False
            elif ans == 'no':
                print('Please Reenter Experiment Name:')
                experiment_name = input()
            else:
                print("Please Enter correct response")
    print(f"if loop exp_dir:{exp_dir}")
    loggrs = os.path.join(exp_dir, 'loggers')
    if not os.path.exists(loggrs):
        os.mkdir(loggrs)
    chckpoints = os.path.join(exp_dir, 'checkpoints')
    if not os.path.exists(chckpoints):
        os.mkdir(chckpoints)
    return loggrs, chckpoints, experiment_name


def experiment(config_file = 'vgg_16.json', training_type = 'transferCNN'):
    ### CAN BE IMPORTED FROM JSON
    config_file = config_file 
    params, device = set_params(config_file)

    #params from json

    file_path = params.LOCATIONS.file_path
    ROOT = params.LOCATIONS.ROOT
    project = params.LOCATIONS.project
    kfold_manifest = params.LOGGING.kfold_manifest
    exp_path = ROOT + project #pre localisation of data
    exp_path = os.getcwd() 
    csv_path = exp_path + '/' + kfold_manifest


    #model params
    model_type = params.PARAMETERS.model_type
    input_dim = params.PARAMETERS.input_dim
    hidden_dim = params.PARAMETERS.hidden_dim

    #HyperParams from json
    start_train = params.HYPERPARAMETERS.train_fold.start
    end_train = params.HYPERPARAMETERS.train_fold.end
    start_val = params.HYPERPARAMETERS.val_fold.start
    end_val = params.HYPERPARAMETERS.val_fold.end
    test_folds = [params.HYPERPARAMETERS.test_fold]

    val_folds = [start_val, end_val]

    lr = params.HYPERPARAMETERS.learning_rate
    classes = params.HYPERPARAMETERS.n_classes
    batch_size = params.HYPERPARAMETERS.batch_size
    max_epochs = params.HYPERPARAMETERS.max_epochs

    loggers, checkpoints, experiment_name = build_experiment_folders()

    NEPTUNE_API_TOKEN ="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczovL3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiODM4ZTI5NTQtMjQzYS00MDMxLWFiMjgtMzU0ODViYjZiNzRkIn0="

    params = {"csv_path": csv_path,
            "file_path" : file_path,
            "model_type" : model_type,
            "input_dim" : input_dim,
            "hidden_dim" : hidden_dim}
    hyperparams = {"start_train": start_train,
                    "end_train": end_train,
                    "start_val": start_val,
                    "end_val": end_val,
                    "test_folds": test_folds,
                    "lr": lr,
                    "classes": classes,
                    "batch_size": batch_size,
                    "max_epochs": max_epochs,
                    "model_type": model_type}

    load_dotenv()
    #LOGGING TOOL
    tensorboard_logger = TensorBoardLogger('tb_logs', name=experiment_name)
    comet_logger = CometLogger(
        workspace=os.environ.get('COMET_WORKSPACE'),  # Optional
        save_dir=loggers,  # Optional
        project_name='qwertimer1/whale',  # Optional
        rest_api_key=os.environ.get('COMET_REST_API_KEY'),  # Optional
        experiment_name=experiment_name)

    neptune_logger = NeptuneLogger(
        api_key= os.getenv('NEPTUNE_API_TOKEN')
        project_name="qwertimer1/whale",
        close_after_fit=False,
        experiment_name=experiment_name,
        params=hyperparams
    )

    #CHECKPOINT SAVING
    checkpoint_callback = ModelCheckpoint(
        filepath=checkpoints,
        save_top_k=True,
        verbose=True,
        monitor='val_loss',
        mode='min'
    )
    #profiler = AdvancedProfiler()


    trainer = pl.Trainer(gpus = 1,
                        max_epochs = max_epochs,
                        #early_stop_callback = True,
                        val_check_interval = 1000,
                        logger = [tensorboard_logger, comet_logger, neptune_logger],
                        checkpoint_callback = checkpoint_callback,
                        #auto_lr_find = True,
                        profiler = True,
                        fast_dev_run = True
                        )

    model = wc(params, hyperparams)
    if training_type == 'audio':
        model = torchlayers.build(model, torch.randn(32, 1, 48000))
    elif training_type == 'transferCNN':
        model = model 
    else:
        model = model

    print(model)
    trainer.fit(model)
    class_names = []
    class_dict = model.classes
    for i in sorted (class_dict.values()) :
        class_names.append(i)
    trainer.test(model)
    #y = model.y_tot
    #preditions = model.pred_tot
    #fig, ax = plt.subplots(figsize=(16, 12))
    #plot_confusion_matrix(y, predictions, normalize=True, ax = ax)

    # Get predictions on external test


    model.freeze()
    test_loader = model.test_dataloader()
    model.cpu()
    y_true, y_pred = [],[]

    for i, (x, y) in enumerate(test_loader):
        x = x.cpu().detach()

        y_hat = model.forward(x)


            #print(pred.shape)
            #print(f"output = {output}" )
            #_, predicted = torch.max(output.data(2), 1)
        y_hat = y_hat.max(1)[1]

        y_hat = y_hat.cpu().detach().numpy()

        y = y.cpu().detach().numpy()

        y_true = np.append(y_true, y)
        y_pred = np.append(y_pred ,y_hat)

        if i == len(test_loader):
            break

    #y_true = np.hstack(y_true)
    #print(y_true)
    #print(f"y_true -- length {len(y_true)}")

    #y_pred = np.hstack(y_pred)
    #print(f"Predicted --- length = {len(y_pred)}")
    #print(y_true)
    # Log additional metrics

    f_score = f1_score(y_true, y_pred)
    print(f_score)
    neptune_logger.experiment.log_metric('f_score', f_score)
    accuracy = accuracy_score(y_true, y_pred)
    neptune_logger.experiment.log_metric('test_accuracy', accuracy)

    # Log charts
    fig, ax = plt.subplots(figsize=(16, 12))
    #plot_confusion_matrix(y_true, y_pred, ax=ax, labels=class_names)
    cm = confusion_matrix(y_true, y_pred)
    cm_display = ConfusionMatrixDisplay(cm, display_labels=class_names)

    disp = cm_display.plot(
                     cmap='Blues', ax=ax)
    plt.xticks(rotation=45)
    plt.show()

    #plt_cm(model, y_pred, y_true, display_labels = class_names, cmap = plt.cm.Blues)
    neptune_logger.experiment.log_image('confusion_matrix', fig)
    neptune_logger.experiment.log_artifact(checkpoints)
    neptune_logger.experiment.stop()


if __name__ == "__main__":
    experiment() 
