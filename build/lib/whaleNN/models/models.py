import torch
import torch.nn as nn
import torch.nn.functional as F
import torchlayers as tl
import pytorch_lightning
import torchaudio
from torchvision import models

class CNN_RNN_model(nn.Module):
    def __init__(self, input_dim, hidden_dim, classes):
        super(CNN_RNN_model, self).__init__()
        self.hidden_dim = hidden_dim
        self.input_dim = input_dim
        self.classes = classes
        self.cnn = nn.Conv1d(1, 128, kernel_size = 80, stride = 4)

        self.lstm = nn.LSTM(self.hidden_dim, self.hidden_dim, num_layers = 5, dropout = 0.4)
        self.fc = nn.Linear(self.hidden_dim,self.classes)

    def forward(self, x):
        x = self.cnn(x)
        x, _ =  self.lstm(x)
        x = self.fc(x[:, -1, :])
        return(F.log_softmax(x, dim=1))

class RNN_model(nn.Module):

    def __init__(self, input_dim, hidden_dim, classes):
        super(RNN_model, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.classes = classes
        self.lstm = nn.LSTM(self.input_dim,self.hidden_dim, num_layers = 5, dropout = 0.4)
        self.fc = nn.Linear(self.hidden_dim, self.classes)

    def forward(self, x):
        x, _ = self.lstm(x)
        x = self.fc(x[:, -1, :])
        return(F.log_softmax(x, dim=1))


class GRU_model(nn.Module):

    def __init__(self, input_dim, hidden_dim, classes):
        super(GRU_model, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.classes = classes
        self.gru = nn.GRU(self.input_dim,self.hidden_dim, num_layers = 5, dropout = 0.4)
        self.fc = nn.Linear(self.hidden_dim, self.classes)

    def forward(self, x):
        x, _ = self.grukkj(x)
        x = self.fc(x[:, -1, :])
        return(F.log_softmax(x, dim=1))


class bidir_model(nn.Module):

    def __init__(self, input_dim, hidden_dim, classes):
        super(bidir_model, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.classes = classes
        
        ####BIDIR Function
        self.gru = nn.GRU(self.input_dim,self.hidden_dim, num_layers = 5, dropout = 0.4)
        self.fc = nn.Linear(self.hidden_dim, self.classes)

    def forward(self, x):
        x, _ = self.grukkj(x)
        x = self.fc(x[:, -1, :])
        return(F.log_softmax(x, dim=1))

class deep_RNN(nn.Module):
    def __init__():
        pass
    def forward(self, x):
        return self.model(x)


class AutoEncoder(torch.nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()
        self.encoder = tl.Sequential(
            tl.StandardNormalNoise(),  # Apply noise to input images
            torch.nn.Conv1d(1, 128,80,4),
            tl.activations.Swish(),  # Direct access to module .activations
            tl.InvertedResidualBottleneck(squeeze_excitation=False),
            tl.AvgPool(),  # shape 64 x 128 x 128, kernel_size=2 by default
            tl.HardSwish(),  # Access simply through tl
            tl.SeparableConv(128),  # Up number of channels to 128
            tl.InvertedResidualBottleneck(),  # Default with squeeze excitation
            torch.nn.ReLU(),
            tl.AvgPool(),  # shape 128 x 64 x 64, kernel_size=2 by default
            tl.DepthwiseConv(256),  # DepthwiseConv easier to use
            # Pass input thrice through the same weights like in PolyNet
            tl.Poly(tl.InvertedResidualBottleneck(), order=3),
            tl.ReLU(),  # all torch.nn can be accessed via tl
            tl.MaxPool(),  # shape 256 x 32 x 32
            tl.Fire(out_channels=512),  # shape 512 x 32 x 32
            tl.SqueezeExcitation(hidden=64),
            tl.InvertedResidualBottleneck(),
            tl.MaxPool(),  # shape 512 x 16 x 16
            tl.InvertedResidualBottleneck(squeeze_excitation=False),
            # Randomly switch off the last two layers with 0.5 probability
            tl.StochasticDepth(
                torch.nn.Sequential(
                    tl.InvertedResidualBottleneck(squeeze_excitation=False),
                    tl.InvertedResidualBottleneck(squeeze_excitation=False),
                ),
                p=0.5,
            ),
            tl.AvgPool(),  # shape 512 x 8 x 8
        )

        # This one is more "standard"
        self.decoder = tl.Sequential(
            tl.Poly(tl.InvertedResidualBottleneck(), order=2),
            # Has ICNR initialization by default after calling `build`
            tl.ConvPixelShuffle(out_channels=512, upscale_factor=2),
            # Shape 512 x 16 x 16 after PixelShuffle
            tl.Poly(tl.InvertedResidualBottleneck(), order=3),
            tl.ConvPixelShuffle(out_channels=256, upscale_factor=2),
            # Shape 256 x 32 x 32
            tl.Poly(tl.InvertedResidualBottleneck(), order=3),
            tl.ConvPixelShuffle(out_channels=128, upscale_factor=2),
            # Shape 128 x 64 x 64
            tl.Poly(tl.InvertedResidualBottleneck(), order=4),
            tl.ConvPixelShuffle(out_channels=64, upscale_factor=2),
            # Shape 64 x 128 x 128
            tl.InvertedResidualBottleneck(),
            tl.Conv(256),
            tl.Dropout(),  # Defaults to 0.5 and Dropout2d for images
            tl.Swish(),
            tl.InstanceNorm(),
            tl.ConvPixelShuffle(out_channels=32, upscale_factor=2),
            # Shape 32 x 256 x 256
            tl.Conv(16),
            tl.Swish(),
            torch.nn.Conv1d(1, 128,80,4)
            # Shape 3 x 256 x 256
        )

    def forward(self, inputs):
        return self.decoder(self.encoder(inputs))





class pretrained(nn.Module):
    """
    Model selector for pretrained CNN classifiers using transfer learning.
    """
    def __init__(self, input_dim, hidden_dim, classes, model_name):
        super(pretrained, self).__init__()
        self.model_name = model_name
        self.hidden_dim = hidden_dim
        self.classes = classes

        self.model_selector()

    def forward(self, x):
        return self.net(x)
        #x = self.classifier(representations)

    def model_selector(self):



        self.net = getattr(models,self.model_name)(pretrained = True)
        self.model_config(feature_extract = True)


    def set_parameter_requires_grad(self, model, feature_extracting):
        if feature_extracting:
            for param in model.parameters():
                param.requires_grad = False

    def model_config(self, feature_extract):
        # Initialize these variables which will be set in this if statement. Each of these
        #   variables is model specific.

        if "resnet" in self.model_name:
            """ Resnet
            """
            self.set_parameter_requires_grad(self.net, feature_extract)
            num_ftrs = self.net.fc.in_features
            self.net.fc = nn.Linear(num_ftrs, self.classes)
            self.input_size = 224


        elif "vgg" in self.model_name:
            """ VGG
            """
            #self.net = models.vgg11_bn(pretrained=use_pretrained)
            self.set_parameter_requires_grad(self.net, feature_extract)
            num_ftrs = self.net.classifier[6].in_features
            self.net.classifier[6] = nn.Linear(num_ftrs,self.classes)
            self.input_size = 224

        elif "squeezenet" in self.model_name:
            """ Squeezenet
            """
            self.set_parameter_requires_grad(self.net, feature_extract)
            self.net.classifier[1] = nn.Conv2d(512, self.classes, kernel_size=(1,1), stride=(1,1))
            self.input_size = 224

        elif "densenet" in self.model_name:
            """ Densenet
            """
            self.set_parameter_requires_grad(self.net, feature_extract)
            num_ftrs = self.net.classifier.in_features
            self.net.classifier = nn.Linear(num_ftrs, self.classes)
            self.input_size = 224

        elif "inception" in self.model_name:
            """ Inception v3
            Be careful, expects (299,299) sized images and has auxiliary output
            """
            self.set_parameter_requires_grad(self.net, feature_extract)
            # Handle the auxilary net
            num_ftrs = self.net.AuxLogits.fc.in_features
            self.net.AuxLogits.fc = nn.Linear(num_ftrs, self.classes)
            # Handle the primary net
            num_ftrs = self.net.fc.in_features
            self.net.fc = nn.Linear(num_ftrs,self.classes)
            self.input_size = 299
