#system imports
import os
from pathlib import Path
from dotenv import load_dotenv
import argparse

#dictionary imports

import json
from easydict import EasyDict as edict

#nn imports
import torch
import pytorch_lightning as pl
from pytorch_lightning.loggers import CometLogger, TensorBoardLogger, NeptuneLogger
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.early_stopping import EarlyStopping


#plotting
from sklearn.metrics import plot_confusion_matrix as plt_cm
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from scikitplot.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score
import numpy as np


#wrapper for torch models
import torchlayers

#personal
from training.lightning_build import whale_classifier as wc
from training.lightning_build import whaleDataModule as whale_dm



def set_params(config_file = "training/configs/config.json"):
    """
    set_params: The set params function pulls in the  json file containing the absolute path locations of all the required files.
    Aswell the function gets the hyperparams and parameters that aare needed for training.
    args:
        config_file : json file containing the attributes

    returns:
        params : all parameters as a dictionary
        default_device : CUDA check
    """
    exp_path = Path.cwd()
    torch.cuda.empty_cache()

    # Load Test Parameters
    with open(str(exp_path) + '/training/configs/' + str(config_file), "r") as f:
        x = json.load(f)
    params = edict(x)
    default_device = torch.device(
        'cuda' if torch.cuda.is_available() else 'cpu')
    print(params)
    return params, default_device

def build_experiment_folders(experiment_name=None):
    """
    Allows user to input an experiment name and appends to the current working directory an experiment folder
    if it doesnt exist and then places a log and checkpoint directory inside.

    returns:
        loggers : logger folder
        chckpoints : checkpoint folder
        experiment_name : name of experiment
    """

    cwd = os.getcwd()
    experiments = os.path.join(cwd, 'experiments')
    if not os.path.exists(experiments):
        os.mkdir(experiments)
    #Check is experiment is being run from command line
    if experiment_name is None:
        experiment_name = input('Experiment Name')
    exp_dir = os.path.join(experiments, experiment_name)

    if not os.path.exists(exp_dir):
        print(exp_dir)
        os.makedirs(exp_dir)
    loggrs = os.path.join(exp_dir, 'loggers')
    if not os.path.exists(loggrs):
        os.mkdir(loggrs)
    chckpoints = os.path.join(exp_dir, 'checkpoints')
    if not os.path.exists(chckpoints):
        os.mkdir(chckpoints)
    return loggrs, chckpoints, experiment_name


def main(config_file = 'vgg_16.json', training_type = 'audio'):
    parser = argparse.ArgumentParser(description='Select Model Config.')

    parser.add_argument('--config_file', default='vgg_16.json',
                    help='path to json config file')

    parser.add_argument('--experiment_name', default='vgg_16',
                    help='Experiment save folder')

    pretrain_configs = ['densenet121.json', 'resnet101.json', 'resnet18.json', 'resnet34.json', 'resnet50.json', 'squeezenet.json', 'vgg_13.json', 'vgg_16_bn.json', 'vgg_16.json', 'vgg_19_bn.json', 'vgg_19.json']
    allowed_models = ['SIMPLE', 'RES', 'SE', 'FULL', 'FULL10', 'FULL15']


    args = parser.parse_args()
    config_file = args.config_file

    if config_file in pretrain_configs:
        training_type = 'transferCNN'
    else:
        training_type = 'audio'

    experiment_name = args.experiment_name
    params, device = set_params(config_file)

    #params from json

    #file_path = params.LOCATIONS.file_path
    ROOT = params.LOCATIONS.ROOT
    project = params.LOCATIONS.project
    kfold_manifest = params.LOGGING.kfold_manifest
    exp_path = ROOT + project #pre localisation of data
    exp_path = os.getcwd()

    csv_path = exp_path + '/' + kfold_manifest


    loggers, checkpoints, experiment_name = build_experiment_folders(experiment_name)


    parameters = {"csv_path": csv_path,
            "file_path" : params.LOCATIONS.file_path,
            "model_type" : params.PARAMETERS.model_type,
            "model_name" : params.PARAMETERS.model_name,
            "input_dim" : params.PARAMETERS.input_dim,
            "hidden_dim" : params.PARAMETERS.hidden_dim,
            "size"       :params.PARAMETERS.size}
    hyperparams = {"start_train": params.HYPERPARAMETERS.train_fold.start,
                    "end_train": params.HYPERPARAMETERS.train_fold.end,
                    "start_val": params.HYPERPARAMETERS.val_fold.start,
                    "end_val": params.HYPERPARAMETERS.val_fold.end,
                    "test_folds": params.HYPERPARAMETERS.test_fold,
                    "lr": params.HYPERPARAMETERS.learning_rate,
                    "classes": params.HYPERPARAMETERS.n_classes,
                    "batch_size": params.HYPERPARAMETERS.batch_size,
                    "max_epochs": params.HYPERPARAMETERS.max_epochs,
                    "model_type": params.PARAMETERS.model_type}    # fix this

    max_epochs = params.HYPERPARAMETERS.max_epochs

    #Get environment variables including API tokens
    load_dotenv()
    #LOGGING TOOL setup
    tensorboard_logger = TensorBoardLogger('tb_logs', name=experiment_name)

    comet_logger = CometLogger(
        workspace=os.environ.get('COMET_WORKSPACE'),  # Optional
        save_dir=loggers,  # Optional
        project_name='qwertimer1/whale',  # Optional
        rest_api_key=os.environ.get('COMET_REST_API_KEY'),  # Optional
        experiment_name=experiment_name)

    neptune_logger = NeptuneLogger(
        api_key= os.getenv('NEPTUNE_API_TOKEN'),
        project_name="qwertimer1/whale",
        close_after_fit=False,
        experiment_name=experiment_name,
        params=hyperparams
    )



    #CHECKPOINT SAVING
    checkpoint_callback = ModelCheckpoint(
        filepath= f'checkpoints/{experiment_name}',
        save_top_k=True,
        verbose=True,
        monitor='val_loss',
        mode='min'
    )
    logger = [tensorboard_logger, comet_logger, neptune_logger]


    trainer = pl.Trainer(gpus = -1,
                        max_epochs = 2,
                        #precision=16,
                        callbacks = [EarlyStopping(monitor='val_loss')],
                        #val_check_interval = 100,
                        logger = logger,
                        checkpoint_callback = checkpoint_callback,
                        #auto_lr_find = True,
                        profiler = True,
                        fast_dev_run = False
                        )
    model = wc(parameters, hyperparams)
    dm = whale_dm(parameters, hyperparams)

    if training_type == 'audio':
        model = torchlayers.build(model, torch.randn(32, 1, 48000))
    elif training_type == 'transferCNN':
        model = model
    else:
        model = model

    trainer.fit(model, dm)
    class_names = []
 #   class_dict = model.classes
 #   for i in sorted (class_dict.values()) :
 #       class_names.append(i)
    trainer.test(datamodule=dm)

    # Get predictions on external test


    ### Set for external testing (Not sure if i want this)
    #print('external testing')
    model.freeze()
    test_loader = dm.test_dataloader()
    model.cpu()
    y_true, y_pred = [],[]
#
    for i, (x, y) in enumerate(test_loader):
        x = x.cpu().detach()
#
        y_hat = model.forward(x)
#
#
        y_hat = y_hat.max(1)[1]
#
        y_hat = y_hat.cpu().detach().numpy()
#
        y = y.cpu().detach().numpy()
#
        y_true = np.append(y_true, y)
        y_pred = np.append(y_pred ,y_hat)
#
        if i == len(test_loader):
            break
#
    # Log additional metrics

    accuracy = accuracy_score(y_true, y_pred)
    neptune_logger.experiment.log_metric('test_accuracy', accuracy)

    # Log charts
    fig, ax = plt.subplots(figsize=(16, 12))
    cm = confusion_matrix(y_true, y_pred)
    cm_display = ConfusionMatrixDisplay(cm, display_labels=class_names)
#
    disp = cm_display.plot(
                     cmap='Blues', ax=ax)
    plt.xticks(rotation=45)
    plt.show(block=False)
    neptune_logger.experiment.log_image('confusion_matrix', fig)
    neptune_logger.experiment.log_artifact(checkpoints)
    neptune_logger.experiment.stop()
    plt.close()





if __name__ == "__main__":
    main()
