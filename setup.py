#import setuptools
#
#setuptools.setup(
#    name="whaleCNN", # Replace with your own username
#    version="0.0.1.dev",
#    author="Timothy Dell",
#    author_email="delltim@gmail.com",
#    description="Whale Classifier",
#    packages=setuptools.find_packages(),
#    classifiers=[
#        "Programming Language :: Python :: 3",
#        "License :: OSI Approved :: MIT License",
#        "Operating System :: OS Independent",
#    ],
#    python_requires='>=3.6',
#)
#

from setuptools import setup

setup(
        name = 'whaleNN',
        version = "0.0.4",
        packages = (['whaleNN', 'whaleNN.dataset', 'whaleNN.experiments', 'whaleNN.models', 'whaleNN.testing', 'whaleNN.utils', 'whaleNN.training', 'whaleNN.exploratory_analysis']),
        entry_points={
        'console_scripts': [
            'whaleNN = whaleNN.__main__:main',
        ],
        }
)
